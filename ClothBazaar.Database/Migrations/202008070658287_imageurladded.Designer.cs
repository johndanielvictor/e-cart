// <auto-generated />
namespace ClothBazaar.Database.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class imageurladded : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(imageurladded));
        
        string IMigrationMetadata.Id
        {
            get { return "202008070658287_imageurladded"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
